package websocket

import (
	"context"
	"encoding/json"
	"log"

	"gitlab.com/telegram_clone/websocket_service/genproto/chat_service"
	grpcPkg "gitlab.com/telegram_clone/websocket_service/pkg/grpc_client"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[int64]*Client

	// Inbound messages from the clients.
	broadcast chan messageChan

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
	grpcClient grpcPkg.GrpcClientI
}

func newHub(grpcClient grpcPkg.GrpcClientI) *Hub {
	return &Hub{
		broadcast:  make(chan messageChan),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[int64]*Client),
		grpcClient: grpcClient,
	}
}

type Message struct {
	ChatType string `json:"chat_type"`
	ChatID   int64  `json:"chat_id"`
	Message  string `json:"message"`
}

type ErrorResponse struct {
	Error string `json:"error"`
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client.userID] = client
			log.Println("New client connected", client.userID)
		case client := <-h.unregister:
			if _, ok := h.clients[client.userID]; ok {
				delete(h.clients, client.userID)
				close(client.send)
			}
			log.Println("Client disconnected", client.userID)
		case data := <-h.broadcast:
			var message Message
			err := json.Unmarshal(data.Message, &message)
			if err != nil {
				h.errorResponse(h.clients[data.UserID], err)
				continue
			}

			// Create message
			_, err = h.grpcClient.MessageService().Create(context.Background(), &chat_service.ChatMessage{
				Message: message.Message,
				ChatId:  message.ChatID,
				UserId:  data.UserID,
			})
			if err != nil {
				h.errorResponse(h.clients[data.UserID], err)
				continue
			}

			// Get Chat members
			result, err := h.grpcClient.ChatService().GetChatMembers(context.Background(), &chat_service.GetChatMembersParams{
				Limit:  1000,
				Page:   1,
				ChatId: message.ChatID,
			})
			if err != nil {
				h.errorResponse(h.clients[data.UserID], err)
				continue
			}

			for _, user := range result.Users {
				if data.UserID == user.Id {
					continue
				}

				client, ok := h.clients[user.Id]
				if ok {
					select {
					case client.send <- data.Message:
					default:
						close(client.send)
						delete(h.clients, client.userID)
					}
				}
			}
		}
	}
}

func (h *Hub) errorResponse(client *Client, err error) {
	if client == nil {
		log.Println("client is nil")
		return
	}

	res, err1 := json.Marshal(ErrorResponse{
		Error: err.Error(),
	})
	if err1 != nil {
		log.Println("failed to marshal")
		return
	}

	select {
	case client.send <- res:
	default:
		close(client.send)
		delete(h.clients, client.userID)
	}
}
